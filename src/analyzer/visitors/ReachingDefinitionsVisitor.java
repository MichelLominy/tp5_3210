package analyzer.visitors;

import analyzer.ast.*;

import java.io.PrintWriter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * This Java code defines a visitor class called ReachingDefinitionsVisitor for a custom parser.
 * The visitor class traverses an abstract syntax tree (AST) and generates optimised code (Single Assignment and
 * Dead-code elimination). The code includes implementations for various types of assignment statements,
 * such as direct assignment, unary assignment, and assignment with arithmetic operations.
 * */
public class ReachingDefinitionsVisitor implements ParserVisitor {
    private PrintWriter m_writer = null;
    private final ArrayList<String> RETURNS = new ArrayList<>();
    private ArrayList<CodeLine> CODE = new ArrayList<>();

    public ReachingDefinitionsVisitor(PrintWriter writer) {
        m_writer = writer;
    }

    @Override
    public Object visit(SimpleNode node, Object data) {
        return null;
    }

    @Override
    public Object visit(ASTProgram node, Object data) {
        node.childrenAccept(this, null);

        computeReachingDefinitions();
        printCode();

        computeSingleAssignment();
        eliminateDeadCode();
        printOptimisedCode();

        return null;
    }

    @Override
    public Object visit(ASTReturnStmt node, Object data) {
        for (int i = 0; i < node.jjtGetNumChildren(); i++) {
            RETURNS.add(((ASTIdentifier) node.jjtGetChild(i)).getValue());
        }
        return null;
    }

    @Override
    public Object visit(ASTBlock node, Object data) {
        node.childrenAccept(this, null);
        return null;
    }

    @Override
    public Object visit(ASTStmt node, Object data) {
        node.childrenAccept(this, null);
        return null;
    }

    @Override
    public Object visit(ASTAssignStmt node, Object data) {
        String assign = (String) node.jjtGetChild(0).jjtAccept(this, null);
        String left = (String) node.jjtGetChild(1).jjtAccept(this, null);
        String right = (String) node.jjtGetChild(2).jjtAccept(this, null);
        String op = node.getOp();

        CODE.add(new CodeLine(op, assign, left, right));

        return null;
    }

    @Override
    public Object visit(ASTAssignUnaryStmt node, Object data) {
        String assign = (String) node.jjtGetChild(0).jjtAccept(this, null);
        String right = (String) node.jjtGetChild(1).jjtAccept(this, null);

        CODE.add(new CodeLine("-", assign,  right, ""));

        return null;
    }

    @Override
    public Object visit(ASTAssignDirectStmt node, Object data) {
        String assign = (String) node.jjtGetChild(0).jjtAccept(this, null);
        String right = (String) node.jjtGetChild(1).jjtAccept(this, null);

        CODE.add(new CodeLine("+", assign, right, ""));

        return null;
    }

    @Override
    public Object visit(ASTExpr node, Object data) {
        return node.jjtGetChild(0).jjtAccept(this, null);
    }

    @Override
    public Object visit(ASTIntValue node, Object data) {
        return "#" + node.getValue();
    }

    @Override
    public Object visit(ASTIdentifier node, Object data) {
        return node.getValue();
    }

    /**
     * Computes the GEN sets for each line of code.
     */
    private void computeGenSets() {
        // TODO exo 1
        for(CodeLine cl : CODE)
            cl.GEN = new Definition(cl.op, cl.ASSIGN, cl.left, cl.right);

    }

    /**
     * Computes the KILL sets for each line of code.
     */
    private void computeKillSets() {
        // TODO exo 1
        for(CodeLine cl : CODE){
            cl.KILL.clear();
            for(CodeLine cl2 : CODE) {
                if (!Objects.equals(cl.ASSIGN, cl2.ASSIGN) || !Objects.equals(cl.left, cl2.left) || !Objects.equals(cl.op, cl2.op)  || !Objects.equals(cl.right, cl2.right)){
                    if (Objects.equals(cl.ASSIGN, cl2.ASSIGN)) {
                        cl.KILL.add(cl2.GEN);
                    }
                }
            }
        }
    }

    /**
     * Computes the Reaching Definitions Analysis for the code.
     */
    private void computeReachingDefinitions() {
        // TODO exo 2

        computeGenSets();computeKillSets();

        for(int i = 0; i < CODE.size(); i++){
            CODE.get(i).ValDef_IN = new HashSet<>();
            CODE.get(i).ValDef_OUT = new HashSet<>();
        }
        Boolean updateInOut = true;

        while(updateInOut){
            updateInOut = false;
            for(int i = 0; i < CODE.size(); i++){
                CodeLine curr = CODE.get(i);

                if( i > 0 && !curr.ValDef_IN.equals(CODE.get(i - 1).ValDef_OUT)) {
                    curr.ValDef_IN = CODE.get(i - 1).ValDef_OUT;
                    updateInOut = true;
                }

                curr.ValDef_OUT.clear();
                curr.ValDef_OUT.addAll(curr.ValDef_IN);
                curr.ValDef_OUT.removeAll(curr.KILL);
                curr.ValDef_OUT.add(curr.GEN);
            }
        }
    }

    /**
     * Optimizes the code by eliminating unnecessary assignments.
     */
    public void computeSingleAssignment() {
        // TODO exo 3
        boolean updateOnCodeLine = true;
        while(updateOnCodeLine) {
            updateOnCodeLine = false;
            for (int i = CODE.size() - 1; i > 0; i--) {
                CodeLine curr = CODE.get(i);
                if (Objects.equals(curr.left, curr.ASSIGN) || Objects.equals(curr.right, curr.ASSIGN)) continue;

                for (int j = i - 1; j >= 0; j--) {
                    CodeLine last = CODE.get(j);
                    if (!Objects.equals(last.ASSIGN, curr.ASSIGN)) {
                        if (Objects.equals(last.left, curr.ASSIGN) || Objects.equals(last.right, curr.ASSIGN)) break;
                    } else {
                        updateOnCodeLine = true;
                        CODE.remove(j);
                        i--;
                    }
                }
            }
        }
    }

    /**
     * Eliminates dead code from the code.
     */
    public void eliminateDeadCode() {
        // TODO exo 3
        ArrayList<String> retDependencyList = new ArrayList<String>();
        if (RETURNS.size() <= 0) {
            CODE.clear();
            return;
        }
        retDependencyList.add(RETURNS.get(0));
        for(int i = CODE.size() - 1; i >= 0; i--){
            CodeLine curr = CODE.get(i);
            if(retDependencyList.contains(curr.ASSIGN)){
                retDependencyList.add(curr.left);
                retDependencyList.add(curr.right);
            }
            else
                CODE.remove(i);
        }
        computeSingleAssignment();

        for(int i = 0; i < CODE.size(); i++){
            CodeLine curr = CODE.get(i);
            for(int j = i + 1; j < CODE.size(); j++){
                CodeLine after = CODE.get(j);
                if(Objects.equals(curr.ASSIGN, after.left)) after.left = curr.GEN.identifier;
                if(Objects.equals(curr.ASSIGN, after.right)) after.right = curr.GEN.identifier;
            }
        }
    }


    public void printCode() {
        int i = 0;
        for (CodeLine code : CODE) {
            String line = code.ASSIGN + " = " + code.left;
            if (!code.right.isEmpty() && !code.right.equals("#0")) {
                line += " " + code.op + " " + code.right;
            }
            m_writer.println("// Bloc " + i);
            m_writer.println(line);
            m_writer.println("// ValDef_IN  : " + sortedDefinitions(code.ValDef_IN));
            m_writer.println("// ValDef_OUT : " + sortedDefinitions(code.ValDef_OUT));
            m_writer.println();
            i++;
        }
    }

    public void printOptimisedCode() {
        m_writer.println("###############################################");
        m_writer.println("Optimised code:");
        for (CodeLine code : CODE) {
            String line =  code.GEN + ": " + code.ASSIGN + " = " + code.left;
            if (!code.right.isEmpty()) {
                line += " " + code.op + " " + code.right;
            }
            m_writer.println(line);
        }
        m_writer.println("###############################################");
    }


    // Helper function to convert a set of Definition objects to a sorted list of strings
    private List<String> sortedDefinitions(Set<Definition> definitions) {
        return definitions.stream()
                .map(Definition::toString)
                .sorted()
                .collect(Collectors.toList());
    }

    /**
     * A struct to store the data of a code line.
     */
    public class CodeLine {
        public String op;
        public String ASSIGN;
        public String left;
        public String right;
        public Definition GEN;
        public Set<Definition> KILL;
        public Set<Definition> ValDef_IN;
        public Set<Definition> ValDef_OUT;

        public CodeLine(String op, String ASSIGN, String left, String right) {
            this.op = op;
            this.ASSIGN = ASSIGN;
            this.left = left;
            this.right = right;
            this.KILL = new HashSet<>();
            this.ValDef_IN = new HashSet<>();
            this.ValDef_OUT = new HashSet<>();
        }
    }
    public int statementNumber = 0;

    /**
     * A struct to store the data of a definition.
     */
    public class Definition {
        public String identifier;
        public String op;
        public String ASSIGN;
        public String left;
        public String right;

        public Definition(String op, String ASSIGN, String left, String right) {
            this.identifier = "d_" + statementNumber++;
            this.op = op;
            this.ASSIGN = ASSIGN;
            this.left = left;
            this.right = right;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof Definition) {
                Definition other = (Definition) obj;
                return this.identifier.equals(other.identifier);
            }
            return false;
        }

        @Override
        public int hashCode() {
            return identifier.hashCode();
        }

        @Override
        public String toString() {
            return identifier;
        }
    }
}